variable "aws_access_key" {
    default = "***your-access-key***"
} 
variable "aws_secret_key" {
    default = "***your-secret-key***"
} 
variable "aws_region" {
    default = "us-east-1"
}

variable "environment" {
  default     = "production"
}

variable "cidr" {
  default = "10.0.0.0/16"
}

variable "ecs_cluster" {
    default = "cluster-test"
}

variable "private_subnets" {
  description = "a list of CIDRs for private subnets"
  default     = ["10.0.0.0/20", "10.0.32.0/20", "10.0.64.0/20"]
}

variable "public_subnets" {
  description = "a list of CIDRs for public subnets"
  default     = ["10.0.16.0/20", "10.0.48.0/20", "10.0.80.0/20"]
}

variable "availability_zones" {
  description = "list of availability zones"
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}