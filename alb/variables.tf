
variable "vpc_id" {
  description = "VPC ID"
}

variable "subnets" {
  description = "Comma separated list of subnet IDs"
}

variable "alb_security_groups" {
  description = "List of security groups"
  default = "arn:aws:acm:us-east-1:00012345678:certificate/abcd-abcd-abcd-1234-abcd"
}

variable "health_check_path" {
  description = "Http path for task health check"
  default     = "/health"
}