provider "aws" {
  region = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

resource "aws_ecs_cluster" "cluster" {
  name = var.ecs_cluster
}

module "vpc" {
  source             = "./vpc"
  cidr               = var.cidr
  private_subnets    = var.private_subnets
  public_subnets     = var.public_subnets
  availability_zones = var.availability_zones
}

module "ecs" {
  source = "./ecs"
}

module "sg" {
  source = "./security-groups"
  vpc_id = module.vpc.id
}

module "s3" {
  source = "./s3"
}

module "alb" {
  source = "./alb"
  vpc_id              = module.vpc.id
  subnets             = module.vpc.public_subnets
  alb_security_groups = [module.sg.alb]
}


