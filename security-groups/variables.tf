variable "vpc_id" {
    description = "VPC ID"
}

variable "container_port" {
    default = 3000
    description = "Ingres and egress port of the container"
}